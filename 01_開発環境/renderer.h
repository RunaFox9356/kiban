//****************************************************************
//
// renderer
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _RENDERER_H_	// このマクロ定義がされてなかったら
#define _RENDERER_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "task.h"
#include "object.h"

//================================================================
// 前方宣言
//================================================================

//================================================================
// class
//================================================================
class CRenderer : public CTask
{
public: /* 定義 */

public: /* メンバ関数 */
	explicit CRenderer(CTask* parent, HWND hWnd);	// コンストラクタ
	~CRenderer() override;	// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Draw();	// 描画

public:
	LPDIRECT3DDEVICE9 GetDevice() { return m_d3dDevice; }	// デバイスの取得
	void SetFog(D3DXCOLOR col, float start, float end, bool flag);	// フォグの設定

	void AddDrawList(CObject* inObject) { m_listDraw.push_back(inObject); }

private: /* メンバ変数 */
	LPDIRECT3D9 m_d3d;	// Direct3D
	LPDIRECT3DDEVICE9 m_d3dDevice;	// デバイス
	HWND m_hWnd;	// ウインドウ
	std::list<CObject*> m_listDraw;	// 描画リスト
};
#endif