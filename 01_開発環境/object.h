//****************************************************************
//
// object
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _OBJECT_H_	// このマクロ定義がされてなかったら
#define _OBJECT_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "state.h"

//================================================================
// class
//================================================================
class CObject
{
public: /* メンバ関数 */
	explicit CObject(CObject* objectParent = nullptr);	// コンストラクタ
	virtual ~CObject();	// デストラクタ

	virtual HRESULT Init() = 0;	// 初期化
	virtual void Uninit();		// 終了
	virtual void Update();		// 更新
	virtual void PreDraw() ;	// 描画準備
	virtual void Draw() = 0;	// 描画
	void AddList(std::list<CObject*> *list) {list->push_back(this);}

public:
	// 位置
	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; Dirty(); }
	const D3DXVECTOR3& GetPos() const { return m_pos; };

	// 向き
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot; Dirty(); }
	const D3DXVECTOR3& GetRot() const { return m_rot; };

	// 移動量
	void SetMove(const D3DXVECTOR3& move) { m_move = move; Dirty(); }
	const D3DXVECTOR3& GetMove() const { return m_move; };

	// 情報変更
	void Dirty() { m_isDirty = true; }			// 変更する
	void ClearDirty() { m_isDirty = false; }	// 変更する
	bool IsDirty() const { return m_isDirty; };	// 変更の有無

protected:
	void SetLocal(const D3DXMATRIX& local) { m_localMtx = local; }	// ローカル座標の取得
	const D3DXMATRIX& GetLocal() const;	// ローカル座標の取得

	void SetWorld(const D3DXMATRIX& World) { m_worldMtx = World; }	// ローカル座標の取得
	const D3DXMATRIX& GetWorld() const;	// ワールド座標の取得

private: /* メンバ変数 */
	D3DXVECTOR3 m_pos;		// 位置
	D3DXVECTOR3 m_rot;		// 向き
	D3DXVECTOR3 m_move;		// 移動量
	D3DXMATRIX m_localMtx;	// ローカル座標
	D3DXMATRIX m_worldMtx;	// ワールド座標
	bool m_isDirty;			// 内容が変更されたかどうか

	CObject* m_parent;				// 親
	std::list<CObject*> m_children;	// 子供達
};
#endif
