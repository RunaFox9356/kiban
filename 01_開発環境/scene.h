//****************************************************************
//
// scene
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _SCENE_H_	//このマクロ定義がされてなかったら
#define _SCENE_H_	//２重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "state.h"

//================================================================
// class
//================================================================
class CScene : public CState
{
public: /* 定義 */

public: /* ↓メンバ関数↓ */
	explicit CScene(CTask* parent);	// コンストラクタ
	~CScene() {};	// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void Update() override;		// 更新

private: /* ↓メンバ変数↓ */
	CState* m_fadeState;	// フェード状況
};

#endif
