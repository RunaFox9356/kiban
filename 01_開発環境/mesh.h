//============================
//
// メッシュ設定ヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _MESH_H_
#define _MESH_H_

#include "main.h"
#include "object.h"
#include "object_3d.h"

#define	EMESHX	(1)
#define	EMESHY	(5)
#define MAX_SIZEMESH (100.0f)
#define MAX_EMESH (2)
#define MOUNTAIN (50.0f)
#define MAXMOVE (10)

#define EMESHMAX (12800)


class CMesh : public CPolygon
{
public:
	virtual void OnHit() {}//メッシュの当たった時の判定

	CMesh(CObject* parent);	// コンストラクタ
	~CMesh() override;

	HRESULT Init() override;//初期化
	void Uninit() override;//破棄
	void Update() override;//更新
	void PreDraw() override;//描画準備
	void Draw() override;//描画

	bool CollisionMesh(D3DXVECTOR3 *pPos);//メッシュの当たり判定つける

	//セッター
	void SetMesh(const int Size);
	void SetOneMeshSize() {	SetMesh(m_nowMesh);}

	void SwitchCollision(bool onCollision) { m_iscollision = onCollision; };
	D3DXVECTOR3 GetMeshSize();
	void SetColor(D3DXCOLOR col);
private:

	void SetVtxMesh(CObject3D::SVertex* pVtx, WORD* pIdx, int nCnt, bool isUp);
	void SetVtxMeshLight();
	void SetVtxMeshSize(int Size);
		
	LPDIRECT3DINDEXBUFFER9 m_idxBuff;         //インデックスバッファ

	int m_xsiz;//面数
	int m_zsiz;//面数
	int m_x;//辺の頂点数
	int m_z;//辺の頂点数
	int m_vtx;//頂点数
	int m_index; //インデックス
	int m_por;
	int m_nowMesh;

	bool m_iscollision;	
};
#endif

