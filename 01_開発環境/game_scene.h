//****************************************************************
//
// game_scene
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _GAME_SCENE_H_	//このマクロ定義がされてなかったら
#define _GAME_SCENE_H_	//２重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "scene.h"

//================================================================
// class
//================================================================
class CGameScene : public CScene
{
public: /* 定義 */

public: /* ↓メンバ関数↓ */
	explicit CGameScene(CTask* parent);	// コンストラクタ
	~CGameScene() {};	// デストラクタ

public:
	HRESULT Init()  override;	// 初期化
	void Update() override;		// 更新

private: /* ↓メンバ変数↓ */
};

#endif
