//****************************************************************
//
// application
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "application.h"
#include "window.h"
#include "renderer.h"
#include "model_data.h"

//================================================================
// 定義
//================================================================
namespace
{
}

//================================================================
// 静的メンバ変数
//================================================================
CApplication* CApplication::m_application = nullptr;

//----------------------------------------------------------------
// インスタンスの取得
//----------------------------------------------------------------
CApplication* CApplication::GetInstance(HINSTANCE hWndInstance, HWND hWnd)
{
	if (m_application == nullptr)
	{
		m_application = new CApplication(hWndInstance, hWnd);
	}

	return m_application;
}

//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CApplication::CApplication(HINSTANCE hWndInstance, HWND hWnd) :
	CTask(),
	m_window(nullptr),
	m_hWndInstance(hWndInstance),
	m_hWnd(hWnd)
{
}

//----------------------------------------------------------------
// デストラクタ
//----------------------------------------------------------------
CApplication::~CApplication()
{
	assert(m_window == nullptr);
}

//----------------------------------------------------------------
// 初期化
//----------------------------------------------------------------
HRESULT CApplication::Init()
{
	// ウィンドウの生成
	m_window = new CWindow(this);

	if (m_window == nullptr)
	{// nullチェック
		return E_FAIL;
	}

	if (FAILED(m_window->Init()))
	{// 初期化失敗
		return E_FAIL;
	}

	return S_OK;
}

//----------------------------------------------------------------
// 終了
//----------------------------------------------------------------
void CApplication::Uninit()
{
	if (m_window != nullptr)
	{// ウィンドウの終了
		m_window->Uninit();
		delete m_window;
		m_window = nullptr;
	}
}

//----------------------------------------------------------------
// 更新
//----------------------------------------------------------------
void CApplication::Update()
{
	// 更新
	CTask::Update();

	// ウィンドウの更新
	m_window->Update();

	// ウィンドウの描画
	m_window->Draw();
}

//----------------------------------------------------------------
// デバイスの取得
//----------------------------------------------------------------
LPDIRECT3DDEVICE9 CApplication::GetDevice() const
{
	return m_window->GetRenderer()->GetDevice();
}
