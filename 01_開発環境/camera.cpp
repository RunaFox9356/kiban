//****************************************************************
//
// camera
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "camera.h"
#include "application.h"
#include "window.h"

//================================================================
// 定義
//================================================================
namespace
{
const float FIELD_OF_VIEW = D3DXToRadian(45.0f);	// 視野角
const float ASPECT_RATIO = (float)CWindow::SCREEN_WIDTH / (float)CWindow::SCREEN_HEIGHT;	// アスペクト比
const float DEFAULT_NEAR = 10.0f;	// ニア
const float DEFAULT_FUR = 40000.0f;	// ファー
const D3DXVECTOR3 DEFAULT_POS = D3DXVECTOR3(0.0f, 50.0f, -500.0f);	// 位置の初期値
}

//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CCamera::CCamera(CObject* parent) :
	CObject(parent),
	m_posR(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
	m_vecU(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
	m_dis(0.0f),
	m_isParallel(false)
{
	ZeroMemory(m_proj, sizeof(m_proj));
	ZeroMemory(m_view, sizeof(m_view));
}

//----------------------------------------------------------------
// デストラクタ
//----------------------------------------------------------------
CCamera::~CCamera()
{
}

//----------------------------------------------------------------
// 初期化
//----------------------------------------------------------------
HRESULT CCamera::Init()
{
	m_posR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_vecU = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	ZeroMemory(m_proj, sizeof(m_proj));
	ZeroMemory(m_view, sizeof(m_view));
	m_dis = 0.0f;
	m_isParallel = false;

	// 位置の設定
	CObject::SetPos(DEFAULT_POS);

	return S_OK;
}

//----------------------------------------------------------------
// 描画準備
//----------------------------------------------------------------
void CCamera::PreDraw()
{
	// ビューの初期化
	D3DXMatrixIdentity(&m_view);

	// ビューの作成
	D3DXMatrixLookAtLH(&m_view,
		&CObject::GetPos(),
		&m_posR,
		&m_vecU);

	// プロジェクションの初期化
	D3DXMatrixIdentity(&m_proj);

	// プロジェクションの作成
	if (!m_isParallel)
	{// 透視投影
		D3DXMatrixPerspectiveFovLH(&m_proj, FIELD_OF_VIEW, ASPECT_RATIO, DEFAULT_NEAR, DEFAULT_FUR);
	}
	else
	{// 平行投影
		float width = (float)CWindow::SCREEN_WIDTH;
		float height = (float)CWindow::SCREEN_HEIGHT;
		D3DXMatrixOrthoLH(&m_proj, width, height, DEFAULT_NEAR, DEFAULT_FUR);
	}
}

//----------------------------------------------------------------
// 描画
//----------------------------------------------------------------
void CCamera::Draw()
{
	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 device = CApplication::GetInstance()->GetDevice();

	// ビューの設定
	device->SetTransform(D3DTS_VIEW, &m_view);

	// プロジェクションの設定
	device->SetTransform(D3DTS_PROJECTION, &m_proj);
}
