//****************************************************************
//
// task
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "task.h"

//================================================================
// 定義
//================================================================
namespace
{
}

//----------------------------------------------------------------
// デフォルトコンストラクタ
//----------------------------------------------------------------
CTask::CTask() :
	m_parent(nullptr)
{
}

//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CTask::CTask(CTask* parent) :
	m_parent(parent)
{
	assert(m_parent != nullptr);
	m_parent->AddChild(this);
}

//----------------------------------------------------------------
// 終了
//----------------------------------------------------------------
void CTask::Uninit()
{
	for (auto c : m_children)
	{
		c->Uninit();
		delete c;
	}

	m_children.clear();
}

//----------------------------------------------------------------
// 更新
//----------------------------------------------------------------
void CTask::Update()
{
	for (auto c : m_children)
	{
		// 更新
		c->Update();
	}
}

//----------------------------------------------------------------
// 描画
//----------------------------------------------------------------
void CTask::Draw()
{
	for (auto c : m_children)
	{
		// 描画
		c->Draw();
	}
}
