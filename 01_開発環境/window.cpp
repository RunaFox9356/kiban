//****************************************************************
//
// window
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "window.h"
#include "renderer.h"
#include "application.h"
#include "scene.h"
#include "title_scene.h"
#include "input.h"

//================================================================
// 定義
//================================================================
namespace
{
LPCTSTR CLASS_NAME = _T("AppClass");	// クラス名
LPCTSTR WINDOW_NAME = _T("kiban");		// キャプション名
}

//================================================================
// 静的プロトタイプ宣言
//================================================================
namespace
{
// ウインドウプロシージャ
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
}

//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CWindow::CWindow(CTask* parent) :
	CTask(parent),
	m_renderer(nullptr),
	m_hWnd(nullptr)
{
}

//----------------------------------------------------------------
// デストラクタ
//----------------------------------------------------------------
CWindow::~CWindow()
{
	assert(m_renderer == nullptr);
}

//----------------------------------------------------------------
// 初期化
//----------------------------------------------------------------
HRESULT CWindow::Init()
{
	HINSTANCE hInstance = CApplication::GetInstance()->GetHWndInstance();

	m_wcex =
	{// ウィンドウクラスの初期化
		sizeof(WNDCLASSEX),
		CS_HREDRAW | CS_VREDRAW,
		WndProc,
		0,
		0,
		hInstance,
		NULL,
		LoadCursor(nullptr, IDC_ARROW),
		(HBRUSH)(COLOR_WINDOW + 1),
		NULL,
		CLASS_NAME,
		NULL
	};

	// ウィンドウクラスの登録
	RegisterClassEx(&m_wcex);

	RECT rect = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

	// 指定したクライアント領域を確保するために必要なウィンドウ座標を計算
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);

	// ウィンドウの作成
	m_hWnd = CreateWindow(
		CLASS_NAME,
		WINDOW_NAME,
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		(rect.right - rect.left),
		(rect.bottom - rect.top),
		NULL,
		NULL,
		hInstance,
		NULL);

	if (!m_hWnd)
	{// 生成失敗
		return E_FAIL;
	}

	// ウィンドウの表示
	ShowWindow(m_hWnd, SW_RESTORE);
	UpdateWindow(m_hWnd);

	// レンダラーの生成
	m_renderer = new CRenderer(this, m_hWnd);

	if (m_renderer == nullptr)
	{// nullチェック
		return E_FAIL;
	}

	if (FAILED(m_renderer->Init()))
	{// 初期化失敗
		return E_FAIL;
	}

	CTask* scene = new CTitleScene(this);
	scene->Init();

	return S_OK;
}

//----------------------------------------------------------------
// 終了
//----------------------------------------------------------------
void CWindow::Uninit()
{
	if (m_renderer != nullptr)
	{// レンダラーの終了
		m_renderer->Uninit();
		delete m_renderer;
		m_renderer = nullptr;
	}

	// ウィンドウを破棄
	DestroyWindow(m_hWnd);

	// ウィンドウクラスの登録を解除
	UnregisterClass(CLASS_NAME, m_wcex.hInstance);
}

//----------------------------------------------------------------
// 更新
//----------------------------------------------------------------
void CWindow::Update()
{
	// レンダラーの更新
	m_renderer->Update();

	// 更新
	CTask::Update();
}

//----------------------------------------------------------------
// 描画
//----------------------------------------------------------------
void CWindow::Draw()
{
	// 描画
	CTask::Draw();
	
	// レンダラーの描画
	m_renderer->Draw();
}

//----------------------------------------------------------------
// サイズの設定
//----------------------------------------------------------------
void CWindow::SetSize(float width, float height)
{
	UINT uFlags = SWP_NOMOVE | SWP_NOZORDER;

	// サイズの設定
	SetWindowPos(m_hWnd, HWND_TOP, 0, 0, (int)width, (int)height, uFlags);
}

//----------------------------------------------------------------
// サイズの取得
//----------------------------------------------------------------
const D3DXVECTOR3 CWindow::GetSize()
{
	RECT rect = { 0, 0, 0, 0 };

	// サイズの取得
	GetWindowRect(m_hWnd, &rect);

	D3DXVECTOR3 size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	size.x = (float)(rect.right - rect.left);
	size.y = (float)(rect.bottom - rect.top);

	return size;
}

//----------------------------------------------------------------
// 描画するかどうかの設定
//----------------------------------------------------------------
void CWindow::SetShowWindow(bool show)
{
	UINT uShow = 0;

	if (show)
	{// 描画する
		uShow = SWP_SHOWWINDOW;
	}
	else
	{// 描画しない
		uShow = SWP_HIDEWINDOW;
	}
	
	UINT uFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | uShow;

	// 描画の設定
	SetWindowPos(m_hWnd, HWND_TOP, 0, 0, 0, 0, uFlags);
}

namespace
{
//----------------------------------------------------------------
// ウィンドウプロシージャ
//----------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		EndPaint(hWnd, &ps);
	}
	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE: // [ESC]キー
			// ウィンドウを破棄
			DestroyWindow(hWnd);
			break;
		}
		break;

	default:
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}
}
