//****************************************************************
//
// object_3d
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "object_3d.h"
#include "application.h"
#include "window.h"
#include "renderer.h"

//================================================================
// 定義
//================================================================
namespace
{
const D3DXVECTOR3 DEFAULT_SIZE = D3DXVECTOR3(100.0f, 100.0f, 0.0f);	// サイズの初期値
}

//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CObject3D::CObject3D(CObject* parent) :
	CPolygon(parent)
{
}

//----------------------------------------------------------------
// デストラクタ
//----------------------------------------------------------------
CObject3D::~CObject3D()
{
}

//----------------------------------------------------------------
// 初期化
//----------------------------------------------------------------
HRESULT CObject3D::Init()
{
	// 初期化
	CPolygon::Init();

	// サイズの設定
	CPolygon::SetSize(DEFAULT_SIZE);

	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 device = CApplication::GetInstance()->GetDevice();

	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();

	// 頂点バッファの生成
	device->CreateVertexBuffer(
		sizeof(SVertex) * CPolygon::NUM_VERTEX,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX,
		D3DPOOL_MANAGED,
		vtxBuff,
		NULL);

	SVertex* vtx = nullptr;	// 頂点情報へのポインタ

	// 頂点情報をロックし、頂点情報へのポインタを取得
	(*vtxBuff)->Lock(0, 0, (void**)&vtx, 0);

	// 位置の取得
	D3DXVECTOR3 pos = CObject::GetPos();

	// サイズの取得
	D3DXVECTOR3 size = CPolygon::GetSize();
	float width = size.x * 0.5f;
	float height = size.y * 0.5f;
	float depth = size.z * 0.5f;

	// 頂点情報の設定
	vtx[0].pos = D3DXVECTOR3(pos.x - width, pos.y + height, pos.z + depth);
	vtx[1].pos = D3DXVECTOR3(pos.x + width, pos.y + height, pos.z + depth);
	vtx[2].pos = D3DXVECTOR3(pos.x - width, pos.y - height, pos.z - depth);
	vtx[3].pos = D3DXVECTOR3(pos.x + width, pos.y - height, pos.z - depth);
	
	// 法線ベクトルの設定
	vtx[0].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	vtx[1].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	vtx[2].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	vtx[3].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	// 色の取得
	D3DXCOLOR color = CPolygon::GetColor();

	// 頂点カラーの設定
	vtx[0].col = color;
	vtx[1].col = color;
	vtx[2].col = color;
	vtx[3].col = color;

	// テクスチャ座標の設定
	vtx[0].tex = D3DXVECTOR2(0.0f, 0.0f);
	vtx[1].tex = D3DXVECTOR2(1.0f, 0.0f);
	vtx[2].tex = D3DXVECTOR2(0.0f, 1.0f);
	vtx[3].tex = D3DXVECTOR2(1.0f, 1.0f);

	// 頂点バッファをアンロックする
	(*vtxBuff)->Unlock();

	return S_OK;
}

//----------------------------------------------------------------
// 終了
//----------------------------------------------------------------
void CObject3D::Uninit()
{
	// 終了
	CPolygon::Uninit();
}

//----------------------------------------------------------------
// 更新
//----------------------------------------------------------------
void CObject3D::Update()
{
	// 更新
	CPolygon::Update();
}

//----------------------------------------------------------------
// 描画準備
//----------------------------------------------------------------
void CObject3D::PreDraw()
{
	Vtx();
	ClearDirty();
}

//----------------------------------------------------------------
// 描画
//----------------------------------------------------------------
void CObject3D::Draw()
{
	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 device = CApplication::GetInstance()->GetDevice();

	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();

	// ライトを無効にする
	device->SetRenderState(D3DRS_LIGHTING, FALSE);

	// 頂点バッファをデータストリームに設定
	device->SetStreamSource(0, (*vtxBuff), 0, sizeof(SVertex));

	// 頂点フォーマットの設定
	device->SetFVF(FVF_VERTEX);

	// テクスチャの設定
	device->SetTexture(0, CPolygon::GetTexture());

	D3DXMATRIX mtx;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&mtx);

	// ワールドマトリックスの設定
	device->SetTransform(D3DTS_WORLD, &mtx);

	// ポリゴンの描画
	device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, CPolygon::NUM_POLYGON);

	// テクスチャの設定
	device->SetTexture(0, nullptr);

	// ライトを有効にする
	device->SetRenderState(D3DRS_LIGHTING, TRUE);
}

//----------------------------------------------------------------
// 色の設定
//----------------------------------------------------------------
void CObject3D::SetColor(const D3DXCOLOR& color)
{
	// 色の設定
	CPolygon::SetColor(color);

	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();

	SVertex* vtx = nullptr;	// 頂点情報へのポインタ

	// 頂点情報をロックし、頂点情報へのポインタを取得
	(*vtxBuff)->Lock(0, 0, (void**)&vtx, 0);

	// 頂点カラーの設定
	vtx[0].col = color;
	vtx[1].col = color;
	vtx[2].col = color;
	vtx[3].col = color;

	// 頂点バッファをアンロックする
	(*vtxBuff)->Unlock();
}


//----------------------------------------------------------------
// 頂点情報の変更
//----------------------------------------------------------------
void CObject3D::Vtx()
{
	if (!IsDirty())
	{// 内容が変更されていない
		return;
	}

	D3DXVECTOR3 rot = CObject::GetRot();
	D3DXMATRIX mtx;	// 計算用マトリックス

					// マトリックスの生成
	D3DXMatrixIdentity(&mtx);

	// マトリックスを回転
	D3DXMatrixRotationZ(&mtx, rot.z);

	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();

	SVertex* vtx = nullptr;	// 頂点情報へのポインタ

	// 頂点情報をロックし、頂点情報へのポインタを取得
	(*vtxBuff)->Lock(0, 0, (void**)&vtx, 0);

	D3DXVECTOR3 pos = CObject::GetPos();
	D3DXVECTOR3 size = CPolygon::GetSize();

	float width = size.x * 0.5f;
	float height = size.y * 0.5f;
	float depth = size.z * 0.5f;

	D3DXVECTOR3 addPos[NUM_VERTEX];

	// 頂点座標の設定
	for (int i = 0; i < NUM_VERTEX; i++)
	{
		D3DXVec3TransformCoord(&addPos[i], &CPolygon::CENTER[i], &mtx);
		vtx[i].pos.x = pos.x + addPos[i].x * width;
		vtx[i].pos.y = pos.y - addPos[i].y * height;
		vtx[i].pos.z = pos.z - addPos[i].y * depth;
	}

	// 頂点バッファをアンロック
	(*vtxBuff)->Unlock();

}