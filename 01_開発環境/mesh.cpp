//****************************************************************
//
// メッシュ設定(まっすぐ)
// Author:hamada ryuuga
//
//****************************************************************


//================================================================
// include
//================================================================
#include "mesh.h"
#include "application.h"
#include "input.h"


//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CMesh::CMesh(CObject* parent) :
	CPolygon(parent)
{
}

//----------------------------------------------------------------
// デストラクタ
//----------------------------------------------------------------
CMesh::~CMesh()
{
}

//----------------------------------------------------------------
// 初期化処理
//----------------------------------------------------------------
HRESULT CMesh::Init()
{

	// 初期化処理
	m_idxBuff = nullptr;         //インデックスバッファ

	m_xsiz = 0;//面数
	m_zsiz = 0;//面数
	m_x = 0;//辺の頂点数
	m_z = 0;//辺の頂点数
	m_vtx = 0;//頂点数
	m_index = 0; //インデックス
	m_por = 0;

	m_iscollision = true;
	CPolygon::SetSize({ 100.0f,100.0f,100.0f });
	
	//テクスチャの読み込み

	SetMesh(10);
	return S_OK;
}

//----------------------------------------------------------------
// 終了処理
//----------------------------------------------------------------
void CMesh::Uninit()
{
	if (m_idxBuff != NULL)
	{
		m_idxBuff->Release();
		m_idxBuff = NULL;
	}
}

//----------------------------------------------------------------
// 更新処理
//----------------------------------------------------------------
void CMesh::Update()
{

}

//----------------------------------------------------------------
// 描画準備
//----------------------------------------------------------------
void CMesh::PreDraw()
{
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans;

	//マトリックス
	D3DXMATRIX mtx = GetWorld();
	//回転
	D3DXVECTOR3 rot = GetRot();
	// 位置の取得
	D3DXVECTOR3 pos = CObject::GetPos();

	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(&mtx);

	// 向きを反映
	// 行列回転関数(第1引数にヨー(y)ピッチ(x)ロール(z)方向の回転行列を作成)
	D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&mtx, &mtx, &mtxRot);

	// 位置を反映
	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&mtx, &mtx, &mtxTrans);

	SetWorld(mtx);
	SetLocal(mtx);
	ClearDirty();
}

//----------------------------------------------------------------
// 描画処理
//----------------------------------------------------------------
void CMesh::Draw()
{

	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetDevice();

	//マトリックス
	D3DXMATRIX mtx = GetWorld();

	// ライトを無効にする
	//pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);


	// ワールド座標行列の設定
	pDevice->SetTransform(D3DTS_WORLD, &mtx);
	// 頂点バッファをデバイスのデータストリームに設定
	pDevice->SetStreamSource(0, *CPolygon::GetBuff(), 0, sizeof(CObject3D::SVertex));
	//インデックスバッファ設定
	pDevice->SetIndices(m_idxBuff);

	// 頂点フォーマットの設定
	pDevice->SetFVF(CObject3D::FVF_VERTEX);

	//テクスチャの設定
	pDevice->SetTexture(0, nullptr);

	// ポリゴンの描画
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, m_vtx, 0, m_por);

	//テクスチャの設定
	pDevice->SetTexture(0, NULL);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
}

//----------------------------------------------------------------
// 当たり判定
// Author: hamada ryuuga
// Aythor: Yuda Kaito
//----------------------------------------------------------------
bool CMesh::CollisionMesh(D3DXVECTOR3 *pPos)
{
	bool bIsLanding = false;
	const int nTri = 3;

	// 位置の取得
	D3DXVECTOR3 getPos = GetPos();
	// サイズの取得
	D3DXVECTOR3 size = CPolygon::GetSize();

	// 頂点座標をロック
	CObject3D::SVertex* vtx = nullptr;

	// デバイスへのポインタの取得
	//LPDIRECT3DDEVICE9 device = CApplication::GetInstance()->GetDevice();

	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();

	// 頂点情報をロックし、頂点情報へのポインタを取得
	(*vtxBuff)->Lock(0, 0, (void**)&vtx, 0);

	//インデックスバッファのロック
	WORD* idx;
	m_idxBuff->Lock(0, 0, (void**)&idx, 0);

	D3DXMATRIX mtxRot, mtxTrans;	// 計算用マトリックス
	D3DXMATRIX mtxWorld;

	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(&mtxWorld);

	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, getPos.x, getPos.y, getPos.z);
	// 行列掛け算関数(第2引数×第3引数を第１引数に格納)
	D3DXMatrixMultiply(&mtxWorld, &mtxWorld, &mtxTrans);

	for (int nCnt = 0; nCnt < m_por; nCnt++)
	{
		D3DXVECTOR3 posLineVec[nTri];

		posLineVec[0] = vtx[idx[nCnt + 0]].pos;
		posLineVec[1] = vtx[idx[nCnt + 1]].pos;
		posLineVec[2] = vtx[idx[nCnt + 2]].pos;
		
		float PosY = posLineVec[0].y;
		if (posLineVec[0].y <= posLineVec[1].y)
		{
			PosY = posLineVec[1].y;
		}
		if (PosY <= posLineVec[2].y)
		{
			PosY = posLineVec[2].y;
		}	

		if ((idx[nCnt + 0] == idx[nCnt + 1]) ||
			(idx[nCnt + 0] == idx[nCnt + 2]) ||
			(idx[nCnt + 2] == idx[nCnt + 1]))
		{//縮退ポリゴンを省き
			continue;
		}

		for (int i = 0; i < nTri; i++)
		{//ベクトル３座標をマトリックスで変換する（乗算）
			D3DXVec3TransformCoord(&posLineVec[i], &posLineVec[i], &mtxWorld);
		}

		int  LineCout = 0;


		for (int i = 0; i < nTri; i++)
		{
			//ベクトルS2 V2												
			D3DXVECTOR3 vecWall = posLineVec[(i + 1) % nTri] - posLineVec[i];

			//ベクトル現在のPOSと始点までの距離
			D3DXVECTOR3 vecPos = *pPos - posLineVec[i];

			//外積計算//辺１
			//float vecLine = utility::Vec2Cross(&vecPos, &vecWall);

			////三角の中に入ってるときの判定向きによって右側か左側か違うため判定を二つ用意する
			//if ((nCnt % 2 == 0 && vecLine >= 0.0f) ||
			//	(nCnt % 2 != 0 && vecLine <= 0.0f))
			//{
			//	LineCout++;
			//}
			//else
			//{
			//	break;
			//}
		}
		if (LineCout == nTri)
		{
			D3DXVECTOR3 V1 = posLineVec[1] - posLineVec[0];
			D3DXVECTOR3 V2 = posLineVec[2] - posLineVec[0];

			D3DXVECTOR3 normal;
			//AとBの法線を求めるやつ
			D3DXVec3Cross(&normal, &V1, &V2);

			//vecB をノーマライズして、長さ 1にする。
			D3DXVec3Normalize(&normal, &normal);

			D3DXVECTOR3 VecA = *pPos - posLineVec[0];
			//プレイヤーの位置補正
			SwitchCollision(true);
			OnHit();
			if (m_iscollision)
			{
				if ((posLineVec[0].y - (normal.x*(pPos->x - posLineVec[0].x) + normal.z*(pPos->z - posLineVec[0].z)) / normal.y) -50.0f <= pPos->y)
				{
					if (pPos->y <= posLineVec[0].y - (normal.x*(pPos->x - posLineVec[0].x) + normal.z*(pPos->z - posLineVec[0].z)) / normal.y)
					{//当たったとき
						bIsLanding = true;
						pPos->y = (posLineVec[0].y - (normal.x*(pPos->x - posLineVec[0].x) + normal.z*(pPos->z - posLineVec[0].z)) / normal.y) + 0.1f;
					
					}
				}
			}
		}
	}
	// 頂点座標をアンロック
	(*vtxBuff)->Unlock();
	// 頂点インデックスをアンロック
	m_idxBuff->Unlock();

	return bIsLanding;
}

//----------------------------------------------------------------
// メッシュの作成
//----------------------------------------------------------------
void CMesh::SetVtxMesh(CObject3D::SVertex* pVtx, WORD* pIdx,int nCnt,bool isUp)
{
	int sign = isUp ? 1 : -1;

	for (int i = 0; i < 3; i++)
	{
		int index = pIdx[nCnt + i];

		pVtx[index].pos.y += (MOUNTAIN * sign);
	}
}

//----------------------------------------------------------------
//法線とIndexの計算
//----------------------------------------------------------------
void CMesh::SetVtxMeshLight()
{
	CObject3D::SVertex* vtx = NULL;
	//インデックスバッファのロック
	WORD* idx;

	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();

	(*vtxBuff)->Lock(0, 0, (void**)&vtx, 0);
	m_idxBuff->Lock(0, 0, (void**)&idx, 0);
	for (int z = 0; z < m_zsiz; z++)
	{
		int linetop = z * (m_x * 2 + 2);
		for (int x = 0; x < m_x; x++)
		{
			int nIdxData = x * 2 + linetop;
			idx[nIdxData + 1] = (WORD)(x + (z * m_x));
			idx[nIdxData] = (WORD)(idx[nIdxData + 1] + m_x);
		}
		//縮退ポリゴン設定
		if (z < m_xsiz - 1)
		{
			idx[m_x * 2 + 0 + linetop] = (WORD)(m_xsiz + m_x*z);
			idx[m_x * 2 + 1 + linetop] = (WORD)(m_x * 2 + m_x * z);
		}
	}
	//---------------------------------------
	//ここから法線
	//---------------------------------------

	//三角の頂点数
	const int tri = 3;

	D3DXVECTOR3 posLineVec[tri];//ベクトル

	for (int nCnt = 0; nCnt < m_por; nCnt++) // プリミティブの数だけまわす。
	{
		//ベクトルを求める
		posLineVec[0] = vtx[idx[nCnt + 0]].pos;
		posLineVec[1] = vtx[idx[nCnt + 1]].pos;
		posLineVec[2] = vtx[idx[nCnt + 2]].pos;

		if ((idx[nCnt + 0] == idx[nCnt + 1]) ||
			(idx[nCnt + 0] == idx[nCnt + 2]) ||
			(idx[nCnt + 2] == idx[nCnt + 1]))
		{
			continue;
		}

		D3DXVECTOR3 V1 = posLineVec[1] - posLineVec[0];
		D3DXVECTOR3 V2 = posLineVec[2] - posLineVec[0];

		D3DXVECTOR3 normal;

		if (nCnt % 2 == 0)
		{
			//AとBの法線を求めるやつ
			D3DXVec3Cross(&normal, &V1, &V2);
		}
		else
		{
			//BとAの法線を求めるやつ
			D3DXVec3Cross(&normal, &V2, &V1);
		}

		//Normalをノーマライズして、長さ 1にする。
		D3DXVec3Normalize(&normal, &normal);

		for (int i = 0; i < tri; i++)
		{//法線計算
			vtx[idx[nCnt + i]].nor += normal;
		}
	}

	for (int nCnt = 0; nCnt < m_vtx; nCnt++)
	{
		//norをノーマライズして、長さ 1にする。
		D3DXVec3Normalize(&vtx[nCnt].nor, &vtx[nCnt].nor);
	}

	// 頂点座標をアンロック
	(*vtxBuff)->Unlock();
	m_idxBuff->Unlock();
}

//----------------------------------------------------------------
// メッシュの枚数決めるやつ
//----------------------------------------------------------------
void CMesh::SetMesh(const int Size)
{
	m_nowMesh = Size;//枚数保存
	SetVtxMeshSize(Size);//サイズ決定
	SetVtxMeshLight();//法線設定
}

//----------------------------------------------------------------
//サイズ初期化
//----------------------------------------------------------------
void CMesh::SetVtxMeshSize(int Size)
{
	CMesh::Uninit();

	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetDevice();
	
	//辺の頂点数
	m_xsiz = Size;
	m_zsiz = Size;
	m_x = m_xsiz + 1;//1多い数字
	m_z = m_zsiz + 1;//1多い数字

	 //頂点数
	m_vtx = m_x* m_z;//頂点数を使ってるよ

	 //インデックス数
	m_index = (2 * m_x * m_zsiz + 2 * (m_zsiz - 1));

	//ポリゴン数
	m_por = m_index - 2;

	CObject3D::SVertex* vtx = nullptr;	// 頂点情報へのポインタ

	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();

	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(CObject3D::SVertex) * m_vtx,
		D3DUSAGE_WRITEONLY,
		CObject3D::FVF_VERTEX,
		D3DPOOL_MANAGED,
		vtxBuff,
		NULL);

	//インデックスバッファ生成
	pDevice->CreateIndexBuffer(sizeof(WORD) * m_index,
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&m_idxBuff,
		NULL);
	
	// 位置の取得
	D3DXVECTOR3 getPos = GetPos();
	D3DXVECTOR3 pos;

	// サイズの取得
	D3DXVECTOR3 size = CPolygon::GetSize();

	// 頂点情報をロックし、頂点情報へのポインタを取得
	(*vtxBuff)->Lock(0, 0, (void**)&vtx, 0);

	// 頂点座標の設定
	for (int i = 0; i < m_vtx; i++)
	{
		vtx[i].pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		float posx = ((i % m_x) - 1.0f);
		float posz = ((i / m_z) - 1.0f)*-1.0f;

		float texU = 1.0f / m_xsiz*(i % m_x);
		float texV = 1.0f / m_zsiz*(i / m_z);

		//めっしゅを真ん中にする補正
		pos = (D3DXVECTOR3(-(posx - 1)*size.x / 2, 0.0f, -posz * size.z / 2)) + getPos;

		//座標の補正
		vtx[i].pos += D3DXVECTOR3(posx*size.x,0.0f, posz * size.z);

		// 各頂点の法線の設定(※ベクトルの大きさは1にする必要がある)
		vtx[i].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		// 頂点カラーの設定
		vtx[i].col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		// テクスチャ座標の設定
		vtx[i].tex = D3DXVECTOR2(texU, texV);

	}

	// 頂点座標をアンロック
	(*vtxBuff)->Unlock();

}

//----------------------------------------------------------------
//セットカラー
//----------------------------------------------------------------
void CMesh::SetColor(D3DXCOLOR col)
{
	CObject3D::SVertex* vtx = nullptr;	// 頂点情報へのポインタ
	// 頂点バッファの取得
	LPDIRECT3DVERTEXBUFFER9* vtxBuff = CPolygon::GetBuff();
	// 頂点情報をロックし、頂点情報へのポインタを取得
	(*vtxBuff)->Lock(0, 0, (void**)&vtx, 0);

	// 頂点座標の設定
	for (int i = 0; i < m_vtx; i++)
	{
		// 頂点カラーの設定
		vtx[i].col = col;
	}

	// 頂点座標をアンロック
	(*vtxBuff)->Unlock();
}

//----------------------------------------------------------------
//サイズ取得
//----------------------------------------------------------------
D3DXVECTOR3 CMesh::GetMeshSize()
{
	D3DXVECTOR3 size = GetSize();
	return D3DXVECTOR3(m_x *size.x, 0.0f, m_z *size.z);
}


