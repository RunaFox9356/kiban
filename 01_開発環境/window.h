//****************************************************************
//
// window
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _WINDOW_H_	//このマクロ定義がされてなかったら
#define _WINDOW_H_	//２重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "task.h"

//================================================================
// 前方宣言
//================================================================
class CRenderer;

//================================================================
// class
//================================================================
class CWindow : public CTask
{
public: /* 定義 */
	static const int SCREEN_WIDTH = 1280;	// スクリーンの幅
	static const int SCREEN_HEIGHT = 720;	// スクリーンの高さ
	
public: /* メンバ関数 */
	explicit CWindow(CTask* parent);	// コンストラクタ
	~CWindow() override;	// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新
	void Draw();	// 描画

public:
	// サイズ
	void SetSize(float width, float height);
	const D3DXVECTOR3 GetSize();

	void SetShowWindow(bool show);	// 描画するかどうかの設定
	CRenderer* GetRenderer() { return m_renderer; }	// レンダラー情報の取得
	HWND GetHWnd() { return m_hWnd; }	// ウィンドウの取得
	
private: /* メンバ変数 */
	CRenderer* m_renderer;	// レンダラー情報
	HWND m_hWnd;			// ウィンドウ
	WNDCLASSEX m_wcex;		// ウィンドウクラス
};

#endif
