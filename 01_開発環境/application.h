//****************************************************************
//
// application
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _APPLICATION_H_	// このマクロ定義がされてなかったら
#define _APPLICATION_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "task.h"

//================================================================
// 前方宣言
//================================================================
class CWindow;

//================================================================
// class
//================================================================
class CApplication : public CTask
{
public: /* 定義 */

public: /* 静的メンバ関数 */
	static CApplication* GetInstance(HINSTANCE hWndInstance = nullptr, HWND hWnd = nullptr);	// インスタンスの取得

private: /* 静的メンバ変数 */
	static CApplication* m_application;	// 自分のインスタンス

public: /* メンバ関数 */
	CApplication(HINSTANCE hWndInstance, HWND hWnd);	// コンストラクタ
	~CApplication() override;	// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新

public:
	CWindow* GetWindow() const { return m_window; }
	HINSTANCE GetHWndInstance() const { return m_hWndInstance; }	// ウィンドウインスタンスの取得
	HWND GetHWnd() const { return m_hWnd; }	// ウィンドウの取得
	LPDIRECT3DDEVICE9 GetDevice() const;	// デバイスの取得

private: /* メンバ変数 */
	CWindow* m_window;	// ウィンドウ情報
	HINSTANCE m_hWndInstance;	// ウィンドウインスタンス
	HWND m_hWnd;	// ウィンドウ
};

#endif
