//****************************************************************
//
// utility
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _UTILITY_H_	// このマクロ定義がされてなかったら
#define _UTILITY_H_	// 二重インクルード防止のマクロ定義

//================================================================
// namespace
//================================================================
namespace utility
{
	// 0.0f〜1.0fの数値を0〜255に変換
	template<typename T>
	inline int Convert256(T value)
	{
		if (std::is_same<T, D3DXCOLOR>::value)
		{
			value.r *= 255.0f;
			value.g *= 255.0f;
			value.b *= 255.0f;
			value.a *= 255.0f;
		}
		else
		{
			value *= 255.0f;
		}
		return static_cast<int>(value);
	}

	// 範囲内をランダムに取得
	// TODO:湯田 シード設定1回で良くね？ int/floatで分けたほうが良くね？
	template<typename T>
	inline T Random(T min, T max)
	{
		std::random_device seed_gen;
		std::mt19937 gen(seed_gen());
		std::uniform_int_distribution<> distribution(0, 32767);
		int randomValue = distribution(gen);
		return ((randomValue * (1.0f / 32767.0f)) * (max - min)) + min;
	}

	// vectorの要素をランダムに取得
	// TODO:湯田 田中ちゃまを質問攻め
	template<typename T>
	inline T RandomElement(const std::vector<T>& element)
	{
		std::random_device seed_gen;
		std::mt19937 gen(seed_gen());
		std::uniform_int_distribution<size_t> distribution(0, element.size() - 1);
		return element[distribution(gen)];
	}

	// 線形補間
	template<typename T>
	inline T Lerp(const T& start, const T& end, float time)
	{
		return start + (end - start) * time;
	}

	// 数値を範囲内に丸める
	template<typename T>
	inline T Clamp(T val, T minVal, T maxVal)
	{
		return max(minVal, min(maxVal, val));
	}

	// vectorの要素を一意なものにする
	template<typename T>
	inline T Unique(std::vector<T>& container)
	{
		container.erase(std::unique(container.begin(), container.end(), container.end()));
	}

	// vectorに重複なく要素を追加する
	template<typename T>
	inline void AddUnique(std::vector<T>& container, const T& val)
	{
		if (std::find(container.begin(), container.end(), val) == container.end())
		{
			container.push_back(value);
		}
	}

	// vectorの全ての要素を連結文字列にする
	template<typename T>
	inline std::string VectorToString(const std::vector<T>& vector)
	{
		std::string result;
		for (const auto& p : vector)
		{
			result += std::to_string(p) + " ";
		}
		return result;
	}

	// 関数の実行時間を計測する
	inline long long ExecutionTime(const std::function<void()>& func)
	{
		auto start = std::chrono::high_resolution_clock::now();
		func();
		auto end = std::chrono::high_resolution_clock::now();
		return std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
	}

	// 角度の正規化
	inline float AngleNormalize(float& angle)
	{
		if (angle > D3DX_PI)
		{
			angle = angle - D3DX_PI * 2.0f;
		}
		else if (angle < -D3DX_PI)
		{
			angle = angle + D3DX_PI * 2.0f;
		}

		return angle;
	}

	// 角度の正規化
	inline D3DXVECTOR3 AngleNormalize(D3DXVECTOR3& rot)
	{
		AngleNormalize(rot.x);
		AngleNormalize(rot.y);
		AngleNormalize(rot.z);

		return rot;
	}

	// 2Dベクトルの外積
	inline float Vec2Cross(D3DXVECTOR3* vec1, D3DXVECTOR3* vec2)
	{
		return vec1->x * vec2->z - vec1->z * vec2->x;
	}

	// 2Dベクトルの内積
	inline float Vec2Dot(D3DXVECTOR3* vec1, D3DXVECTOR3* vec2)
	{
		return vec1->x * vec2->x + vec1->z * vec2->z;
	}

	// 行列の逆行列を変換
	// TODO:湯田 ローカル変数をconst参照するな
	const D3DXMATRIX& InverseMatrixConversion(const D3DXMATRIX& inMtx)
	{
		D3DXMATRIX mtx;

//		mtx._11 = inMtx._11;
		mtx._12 = inMtx._21;
		mtx._13 = inMtx._31;
		mtx._21 = inMtx._12;
//		mtx._22 = inMtx._22;
		mtx._23 = inMtx._32;
		mtx._31 = inMtx._13;
		mtx._32 = inMtx._23;
//		mtx._33 = inMtx._33;

		return mtx;
	}

	// クオータニオンをラジアンに変換する(挙動不安定)
	// TODO:湯田 不安定なものを入れるなばかあほぼけなす
	D3DXVECTOR3 ConvertQuaternionfromRadian(const D3DXQUATERNION& inQuaternion)
	{
		D3DXVECTOR3 rot;
		float z1 = 2.0f * (inQuaternion.x * inQuaternion.y + inQuaternion.z * inQuaternion.w);
		float z2 = inQuaternion.x * inQuaternion.x - inQuaternion.y * inQuaternion.y - inQuaternion.z * inQuaternion.z - inQuaternion.w * inQuaternion.w;
		float z3 = z1 / z2;
		rot.z = atanf(z3);

		float y1 = 2.0f * (inQuaternion.x * inQuaternion.z - inQuaternion.y * inQuaternion.w);
		rot.y = atanf(y1);

		float x1 = 2.0f * (inQuaternion.x * inQuaternion.w + inQuaternion.y * inQuaternion.z);
		float x2 = inQuaternion.x * inQuaternion.x + inQuaternion.y * inQuaternion.y - inQuaternion.z * inQuaternion.z - inQuaternion.w * inQuaternion.w;
		float x3 = x1 / x2;
		rot.x = atanf(x3);

		return rot;
	}


	//	D3DXMATRIX *giftmtx(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXVECTOR3 rot, bool Billboard = false);
	//	D3DXMATRIX *giftmtxQuat(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXQUATERNION Quat, bool Billboard = false);
	//	float easeInSine(float X);
	//	float easeInQuad(float X);
	//	bool is_sjis_lead_byte(int c);
};

#endif
