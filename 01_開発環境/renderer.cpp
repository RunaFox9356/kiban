//****************************************************************
//
// renderer
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "renderer.h"
#include "window.h"
#include "main.h"
#include "camera.h"
#include "object_3d.h"
#include "mesh.h"	
#include "light.h"
#include "object_x.h"

//================================================================
// 定義
//================================================================
namespace
{
const bool FULL_SCREEN = false;	// フルスクリーンにするかどうか
}

//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CRenderer::CRenderer(CTask* parent, HWND hWnd) :
	CTask(parent),
	m_d3d(nullptr),
	m_d3dDevice(nullptr),
	m_hWnd(hWnd)
{
}

//----------------------------------------------------------------
// デストラクタ
//----------------------------------------------------------------
CRenderer::~CRenderer()
{
	assert(m_d3d == nullptr);
	assert(m_d3dDevice == nullptr);
}

//----------------------------------------------------------------
// 初期化
//----------------------------------------------------------------
HRESULT CRenderer::Init()
{
	D3DPRESENT_PARAMETERS d3dpp;
	D3DDISPLAYMODE d3ddm;

	// Direct3Dの作成
	m_d3d = Direct3DCreate9(D3D_SDK_VERSION);

	if (m_d3d == nullptr)
	{// 生成失敗
		return E_FAIL;
	}

	// 現在のディスプレイモードを取得
	if (FAILED(m_d3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)))
	{
		return E_FAIL;
	}

	// デバイスのプレゼンテーションパラメータの設定
	ZeroMemory(&d3dpp, sizeof(d3dpp));							// ワークをゼロクリア
	d3dpp.BackBufferCount = 1;									// バックバッファの数
	d3dpp.BackBufferWidth = CWindow::SCREEN_WIDTH;				// ゲーム画面サイズ(幅)
	d3dpp.BackBufferHeight = CWindow::SCREEN_HEIGHT;			// ゲーム画面サイズ(高さ)
	d3dpp.BackBufferFormat = d3ddm.Format;						// カラーモードの指定
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;					// 映像信号に同期してフリップする
	d3dpp.EnableAutoDepthStencil = TRUE;						// デプスバッファ（Ｚバッファ）とステンシルバッファを作成
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;					// デプスバッファとして16bitを使う
	d3dpp.Windowed = !FULL_SCREEN;								// ウィンドウモード
	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;	// リフレッシュレート
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;	// インターバル

	// ディスプレイアダプタを表すためのデバイスを作成
	// 描画と頂点処理をハードウェアで行なう
	if ((FAILED(m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &m_d3dDevice))) &&
		// 上記の設定が失敗したら
		// 描画をハードウェアで行い、頂点処理はCPUで行なう
		(FAILED(m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &m_d3dDevice))) &&
		// 上記の設定が失敗したら
		// 描画と頂点処理をCPUで行なう
		(FAILED(m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, m_hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &m_d3dDevice))))
	{// 生成失敗
		return E_FAIL;
	}

	// レンダーステートの設定
	m_d3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_d3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_d3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_d3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// サンプラーステートの設定
	m_d3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_d3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_d3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	m_d3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);

	// テクスチャステージステートの設定
	m_d3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	m_d3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_d3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_CURRENT);

	CObject* camera = new CCamera(nullptr);
	camera->Init();

	CObject3D* obj3D = new CObject3D(nullptr);
	obj3D->Init();
	obj3D->SetColor(D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
	obj3D->SetPos(D3DXVECTOR3(0.0f, 10.0f ,0.0f));


	CMesh* mesh = new CMesh(nullptr);
	mesh->Init();
	mesh->SetColor(D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));

	CLight* light = new CLight(nullptr);
	light->Init();

	CObjectX* ObjectX = new CObjectX(nullptr);
	ObjectX->Init();
	ObjectX->SetModelData(CModelData::MODEL_SUN);
	return S_OK;
}

//----------------------------------------------------------------
// 終了
//----------------------------------------------------------------
void CRenderer::Uninit()
{
	if (m_d3dDevice != nullptr)
	{// デバイスの破棄
		m_d3dDevice->Release();
		m_d3dDevice = nullptr;
	}

	if (m_d3d != nullptr)
	{// Direct3Dの破棄
		m_d3d->Release();
		m_d3d = nullptr;
	}
}

//----------------------------------------------------------------
// 描画
//----------------------------------------------------------------
void CRenderer::Draw()
{
	std::list<CObject*> sortListDraw;

	for (auto dl : m_listDraw)
	{
		dl->PreDraw();
		dl->AddList(&sortListDraw);
	}

	// TODO: 湯田＞ソート機能

	//画面クリア ( バックバッファ & Zバッファのクリア )
	m_d3dDevice->Clear(0, NULL, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(0, 0, 0, 0), 1.0f, 0);
	// Direct3Dによる描画の開始
	if (SUCCEEDED(m_d3dDevice->BeginScene()))
	{
		for (auto dl : sortListDraw)
		{
			dl->Draw();
		}

		//CTask::Draw();

		// Direct3Dによる描画の終了
		m_d3dDevice->EndScene();
	}

	m_d3dDevice->Present(NULL, NULL, NULL, NULL);
}

//----------------------------------------------------------------
// フォグの設定
//----------------------------------------------------------------
void CRenderer::SetFog(D3DXCOLOR col, float start, float end, bool flag)
{
	// 有効設定
	m_d3dDevice->SetRenderState(D3DRS_FOGENABLE, flag);

	// 色の設定
	m_d3dDevice->SetRenderState(D3DRS_FOGCOLOR, col);

	// モードの設定 第二引数(範囲指定 : D3DFOG_LINEAR, 密度指定 : D3DFOG_EXP)
	m_d3dDevice->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);

	// 範囲指定(※LINEAR時に指定)
	m_d3dDevice->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&start));
	m_d3dDevice->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&end));

	// 密度指定(※EXP時に指定)
	//float fFogDensity = 0.0001f;
	//m_d3dDevice->SetRenderState(D3DRS_FOGDENSITY, *(DWORD*)(&fFogDensity));
}
