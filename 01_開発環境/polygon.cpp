//****************************************************************
//
// polygon
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "polygon.h"
#include "application.h"

//================================================================
// 定義
//================================================================
const D3DXVECTOR3 CPolygon::CENTER[NUM_VERTEX] =
{// 形成する四角形の基準値
	D3DXVECTOR3(-1.0f, -1.0f, 0.0f),
	D3DXVECTOR3(+1.0f, -1.0f, 0.0f),
	D3DXVECTOR3(-1.0f, +1.0f, 0.0f),
	D3DXVECTOR3(+1.0f, +1.0f, 0.0f),
};

//----------------------------------------------------------------
// コンストラクタ
//----------------------------------------------------------------
CPolygon::CPolygon(CObject* parent) :
	CObject(parent),
	m_vtxBuff(nullptr),
	m_texture(nullptr),
	m_size(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
	m_color(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f))
{
}

//----------------------------------------------------------------
// デストラクタ
//----------------------------------------------------------------
CPolygon::~CPolygon()
{
	assert(m_vtxBuff == nullptr);
	assert(m_texture == nullptr);
}

//----------------------------------------------------------------
// 初期化
//----------------------------------------------------------------
HRESULT CPolygon::Init()
{
	m_vtxBuff = nullptr;
	m_texture = nullptr;
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	return S_OK;
}

//----------------------------------------------------------------
// 終了
//----------------------------------------------------------------
void CPolygon::Uninit()
{
	if (m_vtxBuff != nullptr)
	{// 頂点バッファの破棄
		m_vtxBuff->Release();
		m_vtxBuff = nullptr;
	}

	if (m_texture != nullptr)
	{// テクスチャの破棄
		m_texture->Release();
		m_texture = nullptr;
	}
}
