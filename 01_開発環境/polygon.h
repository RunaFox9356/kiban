//****************************************************************
//
// polygon
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _POLYGON_H_	// このマクロ定義がされてなかったら
#define _POLYGON_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "object.h"


//================================================================
// class
//================================================================
class CPolygon : public CObject
{
public: /* 定義 */
	static const int NUM_VERTEX = 4;	// 頂点の数
	static const int NUM_POLYGON = 2;	// ポリゴンの数
	static const D3DXVECTOR3 CENTER[NUM_VERTEX];	// 形成するポリゴンの基準値

public: /* メンバ関数 */
	explicit CPolygon(CObject* parent);	// コンストラクタ
	~CPolygon() override;		// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了

public:
	LPDIRECT3DVERTEXBUFFER9* GetBuff() { return &m_vtxBuff; }	// 頂点バッファの取得
	LPDIRECT3DTEXTURE9 GetTexture() { return m_texture; }	// テクスチャの取得

	// サイズ
	void SetSize(const D3DXVECTOR3& size) { m_size = size; Dirty(); }
	const D3DXVECTOR3& GetSize() const { return m_size; };

	// 色
	void SetColor(const D3DXCOLOR& color) { m_color = color; }
	const D3DXCOLOR& GetColor() const { return m_color; };

private: /* メンバ変数 */
	LPDIRECT3DVERTEXBUFFER9 m_vtxBuff;	// 頂点バッファ
	LPDIRECT3DTEXTURE9 m_texture;		// テクスチャ
	D3DXVECTOR3 m_size;	// サイズ
	D3DXCOLOR m_color;	// 色
};

#endif
