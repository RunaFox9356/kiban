//****************************************************************
//
// task
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _TASK_H_	// このマクロ定義がされてなかったら
#define _TASK_H_	// 二重インクルード防止のマクロ定義

//================================================================
// class
//================================================================
class CTask
{
public: /* 定義 */

public: /* メンバ関数 */
	CTask();	// デフォルトコンストラクタ
	explicit CTask(CTask* parent);	// コンストラクタ
	virtual ~CTask() {}	// デストラクタ

public:
	virtual HRESULT Init() = 0;	// 初期化
	virtual void Uninit();		// 終了
	virtual void Update();		// 更新
	virtual void Draw();		// 描画

public:
	const CTask* GetParent() const { return m_parent; }	// 親の取得

private:
	CTask* AddChild(CTask* child) { m_children.push_back(child); return child; }	// 子供の追加

private: /* メンバ変数 */
	CTask* m_parent;	// 親
	std::list<CTask*> m_children;	// 子供
};

#endif
