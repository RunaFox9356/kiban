//****************************************************************
//
// object_2d
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _OBJECT_2D_H_	// このマクロ定義がされてなかったら
#define _OBJECT_2D_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "polygon.h"

//================================================================
// class
//================================================================
class CObject2D : public CPolygon
{
public: /* メンバ関数 */
	explicit CObject2D(CObject* parent);	// コンストラクタ
	~CObject2D() override;	// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新
	void PreDraw() override;	// 描画準備
	void Draw() override;		// 描画

public:
	void Vtx();	// 頂点情報の変更

private: /* メンバ変数 */
};

#endif
