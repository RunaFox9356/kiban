//****************************************************************
//
// light
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _LIGHT_H_	// このマクロ定義がされてなかったら
#define _LIGHT_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "object.h"

//================================================================
// class
//================================================================
class CLight : public CObject
{
public: /* 定義 */

public: /* メンバ関数 */
	explicit CLight(CObject* parent);	// コンストラクタ
	~CLight()override;	// デストラクタ

public:
	HRESULT Init()override;		// 初期化
	void Uninit()override;		// 終了
	void Update()override;		// 更新
	void PreDraw() override;	// 描画準備
	void Draw()  override;		// 描画

public:
	void Set(D3DLIGHTTYPE type, D3DXVECTOR3 vec, D3DXCOLOR col, int index);	// 設定
	D3DLIGHT9 Get() { return m_light; }	// ライト情報の取得
	void IsEnable(bool isEnable);	// ライトの有効設定

private: /* メンバ変数 */
	D3DLIGHT9 m_light;	// ライト情報
	int m_index;	// 番号
};

#endif
