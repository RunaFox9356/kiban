//****************************************************************
//
// camera
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _CAMERA_H_	// このマクロ定義がされてなかったら
#define _CAMERA_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "object.h"

//================================================================
// class
//================================================================
class CCamera : public CObject
{
public: /* 定義 */

public: /* メンバ関数 */
	explicit CCamera(CObject* parent);	// コンストラクタ
	~CCamera() override;	// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void PreDraw() override;	// 描画準備
	void Draw() override;		// 描画

public:
	// 注視点
	void SetPosR(const D3DXVECTOR3& posR) { m_posR = posR; }
	const D3DXVECTOR3& GetPosR() const { return m_posR; }

	const D3DXMATRIX& GetProj() const { return m_proj; }	// プロジェクションの取得
	const D3DXMATRIX& GetView() const { return m_view; }	// ビューの取得
	void SetParallel(bool isParallel) { m_isParallel = isParallel; }	// 平行投影かどうか

private: /* メンバ変数 */
	D3DXVECTOR3 m_posR;	// 注視点
	D3DXVECTOR3 m_vecU;	// 上方向ベクトル
	D3DXMATRIX m_proj;	// プロジェクション
	D3DXMATRIX m_view;	// ビュー
	float m_dis;		// 視点から注視点までの距離
	bool m_isParallel;	// 平行投影かどうか
};

#endif
