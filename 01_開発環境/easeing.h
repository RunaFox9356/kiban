//****************************************************************
//
// utility
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _EASEING_H_	// このマクロ定義がされてなかったら
#define _EASEING_H_	// 二重インクルード防止のマクロ定義

//================================================================
// namespace
//================================================================
namespace easeing
{
	
	// sinカーブの値が0.0f〜1.0fに変換
	float SinCurve(int time, float cycle)
	{
		return  (sinf((time * cycle) * (D3DX_PI * 2.0f)) + 1.0f) * 0.5f;
	}

	// cosカーブの値が0.0f〜1.0fに変換
	float CosCurve(int time, float cycle)
	{
		return  (cosf((time * cycle) * (D3DX_PI * 2.0f)) + 1.0f) * 0.5f;
	}

	// カーブの値を範囲内に変換
	float Curve(float curve, float min, float max)
	{
		return (curve * (max - min)) + min;
	}

	//	D3DXMATRIX *giftmtx(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXVECTOR3 rot, bool Billboard = false);
	//	D3DXMATRIX *giftmtxQuat(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXQUATERNION Quat, bool Billboard = false);
	//	float easeInSine(float X);
	//	float easeInQuad(float X);
	//	bool is_sjis_lead_byte(int c);
};

#endif
