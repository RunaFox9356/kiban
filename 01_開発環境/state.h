//****************************************************************
//
// task
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _STATE_H_	//このマクロ定義がされてなかったら
#define _STATE_H_	//２重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "task.h"

//================================================================
// class
//================================================================
class CState : public CTask
{
public: /* 定義 */

private:
	using UPDATE_FUNC = void(CState::*)();	// 名前の変更

public: /* ↓メンバ関数↓ */
	explicit CState(CTask* parent);	// コンストラクタ
	~CState();	// デストラクタ

public:
	virtual HRESULT Init()  override;	// 初期化
	virtual void Uninit() override {}	// 終了
	virtual void Update() override;		// 更新

public:
	virtual void Exit() { m_isEnter = false; }	// 出口。必ず状態終了時にここに通る
	void AttachFunc(const UPDATE_FUNC* inFunc, int num);	// 関数群を付ける
	void Change(int index);	// 変更

private:
	virtual void Enter() { m_isEnter = true; }	// 入口。必ず状態開始時にここに通る

private: /* ↓メンバ変数↓ */
	const UPDATE_FUNC* m_func;	// 関数群
	int m_funcNum;				// 関数数
	bool m_isEnter;				// 入口を通ったか否か
};

#endif
