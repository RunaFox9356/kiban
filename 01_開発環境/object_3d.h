//****************************************************************
//
// object_3d
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
#ifndef _OBJECT_3D_H_	// このマクロ定義がされてなかったら
#define _OBJECT_3D_H_	// 二重インクルード防止のマクロ定義

//================================================================
// include
//================================================================
#include "polygon.h"

//================================================================
// class
//================================================================
class CObject3D : public CPolygon
{
public: /* 定義 */
	struct SVertex
	{// 頂点データ
		D3DXVECTOR3 pos;
		D3DXVECTOR3 nor;
		D3DCOLOR col;
		D3DXVECTOR2 tex;
	};

	static const DWORD FVF_VERTEX = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);	// 頂点フォーマット

public: /* メンバ関数 */
	explicit CObject3D(CObject* parent);	// コンストラクタ
	~CObject3D() override;	// デストラクタ

public:
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新
	void PreDraw() override;	// 描画準備
	void Draw() override;		// 描画

public:
	void SetColor(const D3DXCOLOR& color);	// 色の設定
	void Vtx();	// 頂点情報の変更

private: /* メンバ変数 */
};

#endif
