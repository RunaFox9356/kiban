//****************************************************************
//
// main
// Author : Yuda Kaito
// Author : Katsuki Mizuki
// Author : Hamada Ryuuga
//
//****************************************************************
//================================================================
// include
//================================================================
#include "main.h"
#include "application.h"

//================================================================
// 静的メンバ変数
//================================================================
namespace
{
#ifdef _DEBUG
int s_countFPS;	// FPSカウンタ
#endif // _DEBUG
}

//----------------------------------------------------------------
// メイン
//----------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPSTR /*lpCmdLine*/, int /*nCmdShow*/)
{
	CApplication* pApplication = CApplication::GetInstance(hInstance);

	if (FAILED(pApplication->Init()))
	{// 初期化が失敗した場合
		return -1;
	}

	// 分解能を設定
	timeBeginPeriod(1);

	// フレームカウント初期化
	DWORD dwCurrentTime = 0;
	DWORD dwExecLastTime = timeGetTime();

#ifdef _DEBUG
	DWORD dwFrameCount = 0;
	DWORD dwFPSLastTime = dwExecLastTime;
#endif // _DEBUG

	MSG msg;

	// メッセージループ
	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{// PostQuitMessage()が呼ばれたらループ終了
				break;
			}
			else
			{
				// メッセージの翻訳とディスパッチ
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			dwCurrentTime = timeGetTime();	// 現在の時間を取得

#ifdef _DEBUG
			if ((dwCurrentTime - dwFPSLastTime) >= 500)
			{// 0.5秒ごとに実行
				// FPSを算出
				s_countFPS = dwFrameCount * 1000 / (dwCurrentTime - dwFPSLastTime);
				dwFPSLastTime = dwCurrentTime;	// 現在の時間を保存
				dwFrameCount = 0;
			}
#endif // _DEBUG

			if ((dwCurrentTime - dwExecLastTime) >= (1000 / 60))
			{// 1/60秒経過
				// 現在の時間を保存
				dwExecLastTime = dwCurrentTime;

				// 更新
				pApplication->Update();
#ifdef _DEBUG
				dwFrameCount++;
#endif // _DEBUG
			}
			else
			{
				DWORD a = (1000 / 60) - (dwCurrentTime - dwExecLastTime) - 1;
				assert(a < 1000 / 60);
				Sleep(a);
			}
		}
	}

	if (pApplication != nullptr)
	{// 終了
		pApplication->Uninit();
		delete pApplication;
		pApplication = nullptr;
	}

	// 分解能を戻す
	timeEndPeriod(1);

	return (int)msg.wParam;
}

#ifdef _DEBUG
//----------------------------------------------------------------
// FPS取得
//----------------------------------------------------------------
int GetFPS()
{
	return s_countFPS;
}
#endif // _DEBUG
